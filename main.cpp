#include <iostream>
#include <atomic>
#include <cassert>
#include <string>
#include <vector>
#include <functional>
#include <thread>
#include <cmath>
#include <jack/jack.h>

template<typename Callback_T>
class jack_engine final
{
public:
  jack_engine(
      std::string name
      , int inputs
      , int outputs
      , int rate
      , Callback_T cb)
    : m_callback{cb}
  {
    m_client = jack_client_open(name.c_str(), JackNoStartServer, nullptr);
    if (!m_client)
      throw std::runtime_error("Audio error: no JACK server");

    jack_set_process_callback(m_client, process, this);
    jack_set_sample_rate_callback(m_client, [] (jack_nframes_t r, void *arg) -> int {
      auto& self = *static_cast<jack_engine*>(arg);
      self.m_rate = r;
      return 0;
    }, this);
    jack_on_shutdown(m_client, JackShutdownCallback{}, this);
    jack_set_error_function([](const char* str) {
      std::cerr << "JACK ERROR: " << str << std::endl;
    });
    jack_set_info_function([](const char* str) {
      std::cerr << "JACK INFO: " << str << std::endl;
    });

    for (int i = 0; i < inputs; i++)
    {
      auto in = jack_port_register(
                  m_client, ("in_" + std::to_string(i + 1)).c_str(),
                  JACK_DEFAULT_AUDIO_TYPE, JackPortIsInput, 0);
      assert(in);
      m_inputs.push_back(in);
    }

    for (int i = 0; i < outputs; i++)
    {
      auto out = jack_port_register(
                   m_client, ("out_" + std::to_string(i + 1)).c_str(),
                   JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
      assert(out);
      m_outputs.push_back(out);
    }

    m_rate = jack_get_sample_rate(m_client);

    int err = jack_activate(m_client);
    if (err != 0)
    {
      jack_deactivate(m_client);
      jack_client_close(m_client);
      std::cerr << "JACK error: " << err << std::endl;
      throw std::runtime_error("Audio error: JACK cannot activate");
    }

    {
      auto ports = jack_get_ports(
                     m_client, nullptr, JACK_DEFAULT_AUDIO_TYPE,
                     JackPortIsPhysical | JackPortIsOutput);

      if (ports)
      {
        for (std::size_t i = 0; i < m_inputs.size() && ports[i]; i++)
          jack_connect(m_client, ports[i], jack_port_name(m_inputs[i]));

        jack_free(ports);
      }
    }

    {
      auto ports = jack_get_ports(
                     m_client, nullptr, JACK_DEFAULT_AUDIO_TYPE,
                     JackPortIsPhysical | JackPortIsInput);
      if (ports)
      {
        for (std::size_t i = 0; i < m_outputs.size() && ports[i]; i++)
          jack_connect(m_client, jack_port_name(m_outputs[i]), ports[i]);

        jack_free(ports);
      }
    }
  }

  ~jack_engine()
  {
    must_stop = true;

    // don't do this at home
    while (processing)
      std::this_thread::sleep_for(std::chrono::milliseconds(10));

    jack_deactivate(m_client.load());
    jack_client_close(m_client);
  }

private:
  static int process(jack_nframes_t nframes, void* arg)
  {
    auto& self = *static_cast<jack_engine*>(arg);
    if(self.must_stop)
      return 0;

    const auto inputs = self.m_inputs.size();
    const auto outputs = self.m_outputs.size();

    self.processing = true;
    auto float_input = (float**)alloca(sizeof(float*) * inputs);
    auto float_output = (float**)alloca(sizeof(float*) * outputs);
    for (std::size_t i = 0; i < inputs; i++)
    {
      float_input[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(
                         self.m_inputs[i], nframes);
    }
    for (std::size_t i = 0; i < outputs; i++)
    {
      float_output[i] = (jack_default_audio_sample_t*)jack_port_get_buffer(
                          self.m_outputs[i], nframes);
    }

    self.m_callback(float_input, float_output, (int)inputs, (int)outputs, nframes, self.m_rate);
    self.processing = false;

    return 0;
  }

  Callback_T m_callback;
  std::atomic<jack_nframes_t> m_rate{};
  std::atomic<jack_client_t*> m_client{};
  std::vector<jack_port_t*> m_inputs;
  std::vector<jack_port_t*> m_outputs;
  std::atomic_bool must_stop{false}, stopped{true}, processing{};
};

struct sine_callback
{
  float freq = 330.;
  uint64_t idx = 0;

  void operator()(float** ins, float** outs, int n_in, int n_out, uint32_t frames, int rate)
  {
    for(int c = 0; c < n_out; c++)
    {
      float* channel = outs[c];
      for(uint32_t i = 0; i < frames; i++)
      {
        channel[i] = std::sin(6.28f * freq * (i + idx) / rate);
      }
    }

    idx += frames;
  }
};

int main()
{
  jack_engine<sine_callback> engine("my app", 0, 2, 44100, sine_callback{});
  getchar();
}
